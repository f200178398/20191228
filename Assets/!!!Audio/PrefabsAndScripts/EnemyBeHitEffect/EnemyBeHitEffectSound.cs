﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBeHitEffectSound : MonoBehaviour
{
    private EnemyBeHitEffectSoundPool enemyBeHitEffectSoundPool;
    public AudioSource as_enemyBeHitEffectSound;

    public float _timer;
    public float effectLife=1.5f;

    // Start is called before the first frame update
    void Awake()
    {
        enemyBeHitEffectSoundPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<EnemyBeHitEffectSoundPool>();
        as_enemyBeHitEffectSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
        as_enemyBeHitEffectSound.Play();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time> _timer+effectLife)
        {
            as_enemyBeHitEffectSound.Stop();
            enemyBeHitEffectSoundPool.Recovery(gameObject);
        }
    }
}
