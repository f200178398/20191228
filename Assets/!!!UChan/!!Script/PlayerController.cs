﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Range(0,1)]
    public float Speed;
    public float roteSpeed;
    float gravity = 8;
    float rot = 0;

    Vector3 moveDir = Vector3.zero;
    CharacterController controller;
    Animator anim;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();

        //rig_player = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        GetInput();
        CJump();
    }
    void Movement()
    {
        if (controller.isGrounded)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            {
                anim.SetBool("Running", true);
                anim.SetInteger("Condition", 1);
                moveDir = new Vector3(0, 0, 1);
                moveDir *= Speed;
                moveDir = transform.TransformDirection(moveDir);
                Debug.Log("Run");
            }
            if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
            {
                anim.SetBool("Running", false);
                anim.SetInteger("Condition", 0);
                moveDir = new Vector3(0, 0, 0);
                Debug.Log("isStop");
            }
            if (Input.GetKey(KeyCode.S)) //按下S 進行"後退"動作
            {
                anim.SetInteger("Condition", 3);
                moveDir = new Vector3(0, 0, -1);
                Debug.Log("Back");
            }
            if (Input.GetKeyUp(KeyCode.S)) //放開S 結束"後退"動作
            {
                anim.SetInteger("Condition", 0);
                moveDir = new Vector3(0, 0, 0);
                Debug.Log("stop");
            }
                Debug.Log("isGround");
        }
        rot += Input.GetAxis("Horizontal") * roteSpeed * Time.deltaTime;
        transform.eulerAngles = new Vector3(0, rot, 0);

        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * Time.deltaTime);

        if (Input.GetKey(KeyCode.Q))
        {
            anim.SetBool("Attack_4", true);
            Debug.Log("Q");
        }
    }
    void GetInput()
    {
        if (controller.isGrounded)
        {
            if (Input.GetMouseButton(0))
            {

                if (anim.GetBool("Running") == true )
                {
                    anim.SetBool("Running", false);
                    anim.SetInteger("Condition", 0);
                    
                }
                else if (anim.GetBool("Running") == false)
                {
                    Attacking();
                    Debug.Log("attack");
                }


            }
        }
    }

    void Attacking()
    {
        StartCoroutine(AttackRoutine());
        
    }
    void CJump()
    {
        if (controller.isGrounded)
        {
            if (anim.GetBool("Jumping") == false)
            
                if (Input.GetKey(KeyCode.Space))
                {
                    anim.SetBool("Jumping", true);
                   // anim.SetInteger("Condition", 4);
                    moveDir = new Vector3(0, 6, 0);
                    Debug.Log("jump");
                }
                //if (Input.GetKeyUp(KeyCode.Space))
                //{
                //    //anim.SetBool("Jumping", false);
                //    anim.SetInteger("Condition", 4);
                //    //moveDir = new Vector3(0, 0, 0);

                //}
            

            
        } 

        


    }
    IEnumerator AttackRoutine()
    {

        anim.SetBool("attacking", true);
        anim.SetInteger("Condition", 2);
        yield return new WaitForSeconds(0.78f);
        anim.SetInteger("Condition", 0);
        anim.SetBool("attacking", false);
    }

}
