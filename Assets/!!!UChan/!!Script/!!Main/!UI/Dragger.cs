﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Dragger : MonoBehaviour, IDragHandler , IBeginDragHandler ,IEndDragHandler
{
    public Image itemImage;
    private Vector3 DeltaPosition;
    private Transform parentBeforeDrag;
    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        itemImage.raycastTarget = false;
        DeltaPosition = Input.mousePosition - this.transform.position;
        itemImage.raycastTarget = false;
        parentBeforeDrag = this.transform.parent;
        this.transform.SetParent ( UIMain.Instance().transform);
        //throw new System.NotImplementedException();
        print("onBeginDrag");
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        this.transform.position = Input.mousePosition + DeltaPosition;
        print("Yo");
        //throw new System.NotImplementedException();
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        if(this.transform.parent == UIMain.Instance().transform)
        {
            this.transform.SetParent( parentBeforeDrag);
            
        }
        this.transform.localPosition = Vector3.zero;
        itemImage.raycastTarget = true;
        print("endBeginDrag");
        //hrow new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        itemImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
